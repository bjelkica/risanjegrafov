import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;



public class Risi {
	Vozlisce tocka;
	Platno platno;
	int width;
	int height;
	double x;
	double y;
	double x1;
	double y1;
	double x2;
	double y2;
	Color c1;
	Color c2;
	Graphics g1;
	Graphics g2;
     
	public Risi(Platno platno){
	      this.width=platno.getWidth();
	      this.height=platno.getHeight();
	}
	
	
	public void risiCrto(Graphics g, Crta crta){
		x1= crta.getX1();
		y1= crta.getY1() ;
		x2= crta.getX2() ;
		y2= crta.getY2() ;
		
	
		Graphics2D g1= (Graphics2D) g;
		
		
		
	    g1.draw(new Line2D.Double(x1,y1,x2,y2));
	    
	    g1.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
	    
	}
	
	public void risiTocko(Graphics g, Vozlisce tocka ){
		x=tocka.getX();
		y=tocka.getY();
		
		Graphics2D g2= (Graphics2D) g;
	 
		
		g2.fill(new Ellipse2D.Double(x,y,6,6));
		
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
	
	}
	
	
}
