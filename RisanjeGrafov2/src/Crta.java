
public class Crta {
	public Vozlisce vozlisce1;
	public Vozlisce vozlisce2;
	
public Crta(Vozlisce vozlisce1, Vozlisce vozlisce2){
	this.vozlisce1=vozlisce1;
	this.vozlisce2=vozlisce2;
}
public double getX1(){
	return this.vozlisce1.getX();
}
public double getX2(){
	return this.vozlisce2.getX();
}
public double getY1(){
	return this.vozlisce1.getY();
}
public double getY2(){
	return this.vozlisce2.getY();
}
}
