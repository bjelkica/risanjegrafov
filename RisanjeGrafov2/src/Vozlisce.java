
public class Vozlisce {
	public double x;
	public double y;
	private String oznaka;
	public double odmikx;
	public double odmiky;
	
	public Vozlisce (double x, double y, String oznaka){
		super();
		this.x=x;
		this.y=y;
		this.oznaka=oznaka;
		this.odmikx= 0;
	    this.odmiky=0;
	}
	public double getX(){
		return x;
	}
    
	
	public double getY(){
		return y;
	}
	
	public String getOznaka(){
		return oznaka;
	}
	
	public Vozlisce razlika (Vozlisce u){
		double x= this.getX()-u.getX();
		double y= this.getY() -u.getY();
		Vozlisce raz= new Vozlisce(x,y,"raz");
		return raz;
		
	}
	
	public double absolutna(){
		return Math.sqrt(this.getX()*this.getX()+this.getY()*this.getY());
	}
	
	public Vozlisce dodaj(Vozlisce u){
		double x=this.getX()+u.getX();
		double y= this.getY()+u.getY();
		this.x=x;
		this.y=y;
		return this;
		
	}
	
	/*public Double[] dodaj2(Vozlisce v){
		this.odmik[0]=this.odmik[0]+v.getX();
		this.odmik[1]=this.odmik[1]+v.getY();
		return this.odmik;
	}
	public Double[] razlika2(Vozlisce v){
		this.odmik[0]=this.odmik[0]-v.getX();
		this.odmik[1]=this.odmik[1]-v.getY();
		return this.odmik;
	}
	public Double absolutna2(){
		return Math.sqrt(this.odmik[0]*this.odmik[0]+this.odmik[1]*this.odmik[1]);
	}
	public Double[] krat2(double st){
		this.odmik[0]=this.odmik[0]*st;
		this.odmik[1]=this.odmik[1]*st;
		return this.odmik;
	}*/
	
	public Vozlisce krat(double st){
		this.x=this.getX()*st;
		this.y=this.getY()*st;
		return this;
	}

}
