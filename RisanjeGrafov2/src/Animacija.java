
public class Animacija implements Runnable{
	public Platno pl;
	public Graf graf;
	public int koraki;
	
	
	
	public Animacija(Platno pl,int koraki) {
		super();
		this.pl = pl;
		this.graf=pl.graf;
		this.koraki=koraki;
	}



	@Override
	public void run() {
		
		this.graf.algoritem(graf, pl, koraki);
		
		
	}

	
	
}
