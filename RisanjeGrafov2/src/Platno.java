
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;


import java.io.IOException;



import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;




@SuppressWarnings("serial")
public class Platno extends JPanel{
	int sirina;
	int visina;
	public Graf graf;
	public Risi risi;
	public String ime;
    public ArrayList<Ellipse2D> seznamElips;
    int a;
    Color c1;
    Color c2;
	private JButton gumb;
	Color a1;
	private JButton barva2;
	protected Color a2;
    
    
	
	public Platno (int sirina, int visina,final Graf graf,final JButton gumb,JButton barva2) throws IOException  {
		super();
        this.c1=c1;
        this.c2=c2;
		this.sirina= sirina;
		this.visina= visina;
		this.graf=graf;	
        seznamElips=new ArrayList<Ellipse2D>();
        
        
        Premik pr= new Premik(graf,this);
        addMouseListener(pr);
        addMouseMotionListener(pr);
        
        
        this.gumb=gumb;
        this.gumb.addActionListener(new ActionListener(){
            
 	       @Override
 			
 			public void actionPerformed(ActionEvent e) {
 	    	   
 		     
 	    	   final JColorChooser a = new JColorChooser(gumb.getBackground());
 			   
				
				a1=JColorChooser.showDialog(null, "Barva vozlisca", gumb.getBackground());
				
 			     
 				 
 		         
 				
 			}
 	    	
 	    });
        
		
        this.barva2=barva2;
        this.barva2.addActionListener(new ActionListener(){
            
  	       @Override
  			
  			public void actionPerformed(ActionEvent e) {
  	    	   
  		     
  	    	   final JColorChooser a = new JColorChooser(gumb.getBackground());
  			   
 				
 				a2=JColorChooser.showDialog(null, "Barva povezave", gumb.getBackground());
 				
  			     
  				 
  		         
  				
  			}
  	    	
  	    });
        
	}
	public void spremeni(Dimension d){
		this.setPreferredSize(d);
		repaint();
	}
	public int getWidth(){
		return this.sirina;
	}
	
	public int getHeight(){
		return this.visina;
	}
	

	@Override
	public Dimension getPreferredSize() {
		
		return new Dimension(this.sirina,this.visina);
	}
    
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		seznamElips.clear();
		risi= new Risi(this);
		
		if (this.graf.vozlisca.size()!=0){
			
			
			
			
			for (Crta crta : Graf.povezave){
				if(a2!=null){
					g.setColor(a2);
				}
				else{
					g.setColor(Color.WHITE);
				}
				risi.risiCrto(g, crta);
				
			}
			
			
			for (Vozlisce tocka : this.graf.vozlisca){
			Ellipse2D.Double e= new Ellipse2D.Double(tocka.getX(),tocka.getY(),6,6);
			seznamElips.add(e);
			if(a1!=null){
				g.setColor(a1);
			}
			else{
				g.setColor(Color.GREEN);
			}
			risi.risiTocko(g, tocka);
			
			
			
		}
			
		}
		
	}
public void animiraj(Graf g,int koraki){
	this.graf=g;
	Animacija an= new Animacija(this,koraki);
	new Thread(an).start();
}

	
}
