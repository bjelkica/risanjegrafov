import java.io.*;
import java.util.*;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;



public class Graf {
	public static List<Vozlisce> vozlisca;
	public static List<Crta> povezave;


	
	

	public Graf() {
		this.vozlisca = new ArrayList<Vozlisce>();
		this.povezave = new ArrayList<Crta>();
	}

	public  Graf branje (String ime) throws  IOException {
		Graf g = new Graf();
		Vozlisce vozlisce;
		

		BufferedReader vhod = new BufferedReader (new FileReader(ime));
		// preskocimo prvo vrstico
		String prvaVrstica = vhod.readLine();
		// preberemo stevilo vozlisc
	
		String[] drugaVrstica = vhod.readLine().split(" +");
		int steviloVozlisc = Integer.parseInt(drugaVrstica[1]);
		
		for (int i = 0; i < steviloVozlisc; i++) {
			// preberemo i-to vozlisce
			String[] polja = vhod.readLine().split(" +");
			 //dodamo vozlisce v g
			 if (Double.parseDouble(polja[3])==0.0000 | Double.parseDouble(polja[4])==0.0000){
				 Random r= new Random();
				 vozlisce= new Vozlisce(r.nextDouble(), r.nextDouble(),polja[2]);
				 g.vozlisca.add(vozlisce);
			 }
			 else {
			 vozlisce= new Vozlisce(Double.parseDouble(polja[3]),Double.parseDouble(polja[4]),polja[2]);			 
			 g.vozlisca.add(vozlisce);
			
			 }
			 
		}
		// preskocimo Arcs
		String arcsVrstica = vhod.readLine();
		
		// beremo povezave dokler ni konec datoteke		
		while(vhod.ready()){
			// preberemo povezavo
			String[] polje = vhod.readLine().split(" +");
			
			
			// tu se lahko zgodi prazna vrstica
			if (polje.length==1){
				break;
			}
			
			else{
			
			
			Crta c =new Crta(g.vozlisca.get(Integer.parseInt(polje[1])-1),g.vozlisca.get(Integer.parseInt(polje[2])-1));
			g.povezave.add(c);
			}
		
		}
		vhod.close();
		return g;
		
		
	}	
	
	
	public void algoritem(final Graf graf,final Platno pl,final int koraki){
		
		
		for (Vozlisce v: this.vozlisca){
			v.x=v.getX()*pl.getWidth();
			v.y=v.getY()*pl.getHeight();
		}
		
		

			
				
				double ohladi=0.8;
				int n= graf.vozlisca.size();
				double s=pl.getWidth()*pl.getHeight();
				final double k=Math.sqrt(s/n)+150 ;
				final double t= pl.getWidth()/10;
						
		
				
				for (int i=1;i<=koraki;i++){
		
					
					for(Vozlisce m: graf.vozlisca){
						m.odmikx=0;
						m.odmiky=0;
					}
					for(Vozlisce v:graf.vozlisca){
						
						for(Vozlisce u:graf.vozlisca){
							if(u!=v){
								
								double deltax=(v.getX()-u.getX());
								double deltay=(v.getY()-u.getY());
								double absdelta= Math.sqrt(deltax*deltax+deltay*deltay);
								v.odmikx += (deltax/absdelta)*fr(absdelta,k);
								v.odmiky+= (deltay/absdelta)*fr(absdelta,k);
								
							}
						}
					}
					for (Crta e: graf.povezave){
						double delx= e.getX1()-e.getX2();
						double dely=e.getY1()-e.getY2();
						double absdel=Math.sqrt(delx*delx+dely*dely);
						
						e.vozlisce1.odmikx -=(delx/absdel)*fa(absdel,k);
						e.vozlisce1.odmiky -=(dely/absdel)*fa(absdel,k);
						e.vozlisce2.odmikx +=(delx/absdel)*fa(absdel,k);
						e.vozlisce2.odmiky +=(dely/absdel)*fa(absdel,k);
						
					}
							

						for (Vozlisce w: graf.vozlisca){
							
							double absodmik= Math.sqrt(w.odmikx*w.odmikx + w.odmiky*w.odmiky);
							
							
							w.x= w.x + (w.odmikx/absodmik)*Math.min(absodmik,t*ohladi);
							
							w.y= w.y +(w.odmiky/absodmik)*Math.min(absodmik,t*ohladi);
							
							if(w.x<0 ){
								w.x=1;
							}
							if(w.x>pl.getWidth()){
								w.x=pl.getWidth()-5;
							}
							if(w.y<0 ){
								w.y=1;
							}
							if(w.y>pl.getHeight()){
								w.y=pl.getHeight()-5;
							}
							
							
							
							
							
						}
						ohladi*=0.8;
						
						try {
							pl.repaint();
							Thread.sleep(500);
						} catch (InterruptedException e1) {
							 
							e1.printStackTrace();
						}
						
					
						
					}
				
					
					
			
						
					
				
						
					}
			
					
					
				
				
		
		
	
	   
	
      
    
	public double fa(double x, double k){
		return x*x/k;
		
	}
	public double fr(double x, double k){
		return k*k/x;
	}
	
		

	//}

}
