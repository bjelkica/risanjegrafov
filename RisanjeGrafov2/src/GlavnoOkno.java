import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;





@SuppressWarnings("serial")
public class GlavnoOkno extends JFrame {
	GlavnoOkno okno;
	public JButton gumb;
	public String ime;
	public Graf graf;
	public Platno platno;
	public JFileChooser izbor;
	public JTextField korak;
	public JLabel oznaka;
	public String iter;
	public JColorChooser a;
	public JButton barva2;
	public Color c1;
	public Color c2;
	public JButton barva1;
	private JTextField sir;
	
	
	
	
	public GlavnoOkno() throws IOException  {
		super();

		setTitle("Risanje grafov");
		setLayout(new GridBagLayout());
		this.getContentPane().setBackground(Color.BLACK);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		izbor= new JFileChooser();
	    izbor.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
	   
	    sir= new JTextField(30);
	    sir.setBackground(Color.WHITE);
        sir.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				String i = sir.getText();
				graf=new Graf();
				try {
					GlavnoOkno okno= new GlavnoOkno();
					platno= new Platno(800,800,graf,barva1,barva2);
					platno.setBackground(Color.BLACK);
					GridBagConstraints c = new GridBagConstraints();
					c = new GridBagConstraints();
					c.gridx = 2;
					c.gridy = 2;
					c.fill = GridBagConstraints.BOTH;
					okno.add(platno, c);
					
					osvezi();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			
			}
        	
        });
	   
	    korak= new JTextField();
	    korak.setBackground(Color.WHITE);
        korak.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				iter=korak.getText();
				
				
			}
        	
        });
	   
	    
	    this.oznaka= new JLabel();
	    this.oznaka.setText("Stevilo iteracij: ");
	    this.oznaka.setForeground(Color.WHITE);
	   
	   
	    
	    
	    barva1=new JButton("Barva vozlisc");
	    barva1.setBackground(Color.WHITE);
	    
	    
	    barva2=new JButton("Barva povezav");
	    barva2.setBackground(Color.WHITE);
	    
	    
	    
		
		this.gumb= new JButton("Odpri datoteko");
		this.gumb.setBackground(Color.WHITE);
		this.gumb.addActionListener(new ActionListener(){
			public  void actionPerformed(ActionEvent e){
			         try {
						okno= new GlavnoOkno();
					} catch (IOException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
			    FileNameExtensionFilter filter = new FileNameExtensionFilter("Pajek","paj");
				izbor.setFileFilter(filter);
				
				
					int vrni = izbor.showOpenDialog(okno);
					
					if(vrni==JFileChooser.APPROVE_OPTION){
						File file=izbor.getSelectedFile();
						ime= file.getName();
						
						try {
							 Graf g = graf.branje(ime);
							 if(stevilo(iter)){
								 if (pozstevilo(Integer.parseInt(iter))){
								 platno.animiraj(g,Integer.parseInt(iter));
								 repaint(); 
								 }
							 }
							 else{
			                 platno.animiraj(g,1);
							 repaint();
							 }
								
							
	                        
						
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
		
				}
			}
	});	
		
			
	
		GridBagConstraints c= new GridBagConstraints();
		c.gridx=2;
		c.gridy=1;
		add(this.gumb,c);
		
		
		graf= new Graf();
	    platno= new Platno(700,700,graf,barva1,barva2);
	
		platno.setBackground(Color.BLACK);
		c = new GridBagConstraints();
		c.fill=GridBagConstraints.BOTH;
		c.gridx=2;
		c.gridy=2;
		
		add(platno,c);
		
		c=new GridBagConstraints();
		c.gridx=1;
		c.gridy=1;
		c.weightx=1;
		c.ipadx=60;
		c.ipady=5;
		c.anchor = GridBagConstraints.WEST;
		add(this.korak,c);
		
		c= new GridBagConstraints();
		c.gridx=0;
		c.gridy=1;
		oznaka.setFont(new Font("Dialog",Font.BOLD,13));
		add(this.oznaka,c);
		
		c= new GridBagConstraints();	
		c.gridx=3;
		c.gridy=1;
		add(barva1,c);
		
		c= new GridBagConstraints();
		c.gridx=4;
		c.gridy=1;
		add(barva2,c);
		
		c= new GridBagConstraints();
		c.gridx=4;
		c.gridy=0;
		add(sir,c);
		
	}
  
	public static boolean stevilo(String str)  
	{  
	  try  
	  {  
	    int d = Integer.parseInt(str);  
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;  
	  }  
	 
	return true;
	}
	
	public static boolean pozstevilo(int i){
		if(i>=0){
			return true;
		}
		else{
			return false;
		}
	}
public void osvezi(){
	SwingUtilities.updateComponentTreeUI(this);
}


	}

		
	

