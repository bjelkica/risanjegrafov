import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;


public class Premik extends MouseAdapter{

	private Graf g;
	private Platno pl;
    public Point zadnja;
    public int index;
	public int x;
	public int y;
	
	public Premik(Graf g, Platno pl) {
		super();
		this.g=g;
		this.pl=pl;
		
		
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		
		super.mouseDragged(e);
		if (zadnja!=null){
			double dx=e.getX()-zadnja.x;
			double dy=e.getY()-zadnja.y;
			Vozlisce v= g.vozlisca.get(index);
			v.x+=dx;
			v.y+=dy;
			zadnja.x+=dx;
			zadnja.y+=dy;
			pl.repaint();
		}
		
	}

	@Override
	public void mousePressed(MouseEvent f) {
		for (Ellipse2D el:pl.seznamElips){
			if(el.contains(f.getX(),f.getY())){
				zadnja= new Point(f.getX(),f.getY());
				index=pl.seznamElips.indexOf(el);
				break;
				
			}
		}
		
	
		super.mousePressed(f);
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		zadnja=null;
		super.mouseReleased(arg0);
	}


	

}
