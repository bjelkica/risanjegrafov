RISANJE GRAFOV

Opis:
Risanje grafov je projekt pri predmetu Programiranje 2 v javi. 

Program najprej prebere datoteko v formatu paj (ki se mora nahajati v isti mapi kot projekt) in potem nari�e dani graf. �e so za graf vozli��a �e 
podana, jih uporabi, sicer ustvari nova naklju�na.

V vnosno polje z oznako "Stevilo iteracij" uporabnik vpi�e pozitivno �tevilo , ki dolo�a stevilo iteracij, ki jih izvede algoritem 
(Fruchtermann-Reingoldov algoritem) in jo potrdi s tipko Enter. Privzeto �tevilo iteracij je 1.

Gumba Barva vozlisc in Barva povezav omogo�ata izbor barve za vozli��a in za povezave.

Algoritem omogo�a tudi ro�ni premik vozli�� z mi�ko.


Vsebina repozitorija:
- razredi:
Animacija(prika�e posamezne korake algoritma)
Aplikacija(po�ene program)
Crta
GlavnoOkno
Graf(prebere datoteko in ustrezno spremeni graf)
Platno
Premik(za ro�ni premik vozli��)
Risi
Vozlisce
-primeri datotek:
abc.paj
str.paj
